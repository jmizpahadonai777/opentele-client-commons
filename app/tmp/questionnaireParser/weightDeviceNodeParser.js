(function () {
    'use strict';
    var weightDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.weightDeviceNodeParser', [
        'opentele-commons.deviceListeners'
    ]);
    var weightDeviceNodeParserService = function ($templateCache, parserUtils, nativeService, weightListener) {
        var OMIT = 'Omit';
        var NEXT = 'Next';
        var WEIGHT = 'weight';
        var generateRepresentation = function (node, nodeModel) {
            var nodeTemplate = parserUtils.getNodeTemplate('weightDeviceNode.html');
            var leftButton = {
                text: OMIT,
                nextNodeId: node.nextFail
            };
            var rightButton = {
                text: NEXT,
                nextNodeId: node.next,
                validate: function (scope) {
                    var isValueEntered = function () { return scope.nodeModel.weight !== undefined; };
                    return isValueEntered();
                },
                clickAction: function (scope) {
                    var weightName = node.weight.name;
                    var weightType = node.weight.type;
                    var weightValue = scope.nodeModel.weight;
                    scope.outputModel[weightName] = {
                        name: weightName,
                        type: weightType,
                        value: weightValue
                    };
                    var deviceName = node.deviceId.name;
                    var deviceType = node.deviceId.type;
                    var deviceId = scope.nodeModel.deviceId;
                    scope.outputModel[deviceName] = {
                        name: deviceName,
                        type: deviceType,
                        value: deviceId
                    };
                }
            };
            var representation = {
                nodeTemplate: nodeTemplate,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        var parseNode = function (node, nodeMap, outputModel) {
            var nodeModel = {
                heading: "WEIGHT",
                info: node.text
            };
            var eventListener = weightListener.create(nodeModel);
            var nativeEventCallback = function (message) {
                if (message.measurementType !== WEIGHT) {
                    return;
                }
                eventListener(message.event);
            };
            nativeService.subscribeToMultipleMessages('deviceMeasurementResponse', nativeEventCallback);
            nativeService.addDeviceListener(WEIGHT);
            var representation = generateRepresentation(node, nodeModel);
            return representation;
        };
        return parseNode;
    };
    weightDeviceNodeParser.service('weightDeviceNodeParser', weightDeviceNodeParserService);
}());
//# sourceMappingURL=weightDeviceNodeParser.js.map