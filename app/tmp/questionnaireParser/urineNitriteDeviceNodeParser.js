(function () {
    'use strict';
    var nitriteUrineDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.nitriteUrineDeviceNodeParser', []);
    var service = function ($templateCache, parserUtils) {
        var parseNode = function (node) {
            var nodeModel = {
                heading: node.text,
                measurementSelections: [
                    'URINE_LEVEL_NEGATIVE', 'URINE_LEVEL_POSITIVE'
                ]
            };
            var leftButton = {
                text: "Omit",
                nextNodeId: node.nextFail
            };
            var nodeName = node.nitriteUrine.name;
            var formName = 'inputForm_' + parserUtils.hashCode(nodeName);
            var rightButton = {
                text: "Next",
                nextNodeId: node.next,
                clickAction: function (scope) {
                    var nodeName = node.nitriteUrine.name;
                    var radix = 10;
                    scope.outputModel[nodeName] = {
                        name: nodeName,
                        type: node.nitriteUrine.type,
                        value: parseInt(scope.nodeModel.measurement, radix)
                    };
                },
                validate: function (scope) { return scope[formName].$dirty; }
            };
            var template = parserUtils.getNodeTemplate('urineLevel.html');
            template = parserUtils.replaceAll(template, '#form_name#', formName);
            var representation = {
                nodeTemplate: template,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        return parseNode;
    };
    nitriteUrineDeviceNodeParser.service('nitriteUrineDeviceNodeParser', service);
}());
//# sourceMappingURL=urineNitriteDeviceNodeParser.js.map