(function () {
    'use strict';
    var assignmentNodeParser = angular.module('opentele-commons.questionnaireParser.assignmentNodeParser', []);
    assignmentNodeParser.service('assignmentNodeParser', function (nodesParser) {
        var parseAssignmentNode = function (node, nodeMap, outputModel) {
            var variableName = node.variable.name;
            outputModel[variableName] = {
                name: variableName,
                value: node.expression.value,
                type: node.variable.type
            };
            var nextNodeId = node.next;
            return nodesParser.parse(nextNodeId, nodeMap, outputModel);
        };
        return parseAssignmentNode;
    });
}());
//# sourceMappingURL=assignmentNodeParser.js.map