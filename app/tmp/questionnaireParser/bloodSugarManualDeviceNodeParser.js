(function () {
    'use strict';
    var bloodSugarManualDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.bloodSugarManualDeviceNodeParser', []);
    var bloodSugarService = function ($templateCache, parserUtils) {
        var parseNode = function (node) {
            var nodeModel = {
                heading: node.text
            };
            var leftButton = {
                text: "Omit",
                nextNodeId: node.nextFail
            };
            var rightButton = {
                text: "Next",
                nextNodeId: node.next,
                validate: function (scope) { return scope.bloodSugarForm.count.$valid; },
                clickAction: function (scope) {
                    var nodeName = node.bloodSugarMeasurements.name;
                    var isBeforeMeal = scope.nodeModel.bloodSugarManualBeforeMeal;
                    var isAfterMeal = scope.nodeModel.bloodSugarManualAfterMeal;
                    var timestamp = new Date().toISOString();
                    scope.outputModel[nodeName] = {
                        name: nodeName,
                        type: 'BloodSugarMeasurements',
                        value: {
                            measurements: [{
                                    'result': scope.nodeModel.bloodSugarManualMeasurement,
                                    'isBeforeMeal': isBeforeMeal,
                                    'isAfterMeal': isAfterMeal,
                                    'timeOfMeasurement': timestamp
                                }],
                            transferTime: timestamp
                        }
                    };
                }
            };
            var representation = {
                nodeTemplate: parserUtils.getNodeTemplate('bloodSugarManualDeviceNode.html'),
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        return parseNode;
    };
    bloodSugarManualDeviceNodeParser.service('bloodSugarManualDeviceNodeParser', bloodSugarService);
}());
//# sourceMappingURL=bloodSugarManualDeviceNodeParser.js.map