(function () {
    'use strict';
    var delayNodeParser = angular.module('opentele-commons.questionnaireParser.delayNodeParser', []);
    delayNodeParser.service('delayNodeParser', function ($interval, parserUtils, nodesParser) {
        var parseNode = function (node, nodeMap) {
            var onTimerStopped = function (scope) {
                scope.nextNode(node.next, nodesParser, nodeMap);
            };
            var nodeModel = {
                nodeId: node.nodeName,
                heading: node.displayTextString,
                count: (node.countUp === true) ? 0 : node.countTime,
                countTime: node.countTime,
                countUp: node.countUp,
                onTimerStopped: onTimerStopped
            };
            var representation = {
                nodeTemplate: parserUtils.getNodeTemplate('delayNode.html'),
                nodeModel: nodeModel
            };
            return representation;
        };
        return parseNode;
    });
}());
//# sourceMappingURL=delayNodeParser.js.map