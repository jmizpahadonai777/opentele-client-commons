(function () {
    'use strict';
    var endNodeParser = angular.module('opentele-commons.questionnaireParser.endNodeParser', []);
    endNodeParser.service('endNodeParser', function () {
        var parseNode = function (node) {
            return {
                isEndNode: true
            };
        };
        return parseNode;
    });
}());
//# sourceMappingURL=endNodeParser.js.map