(function () {
    'use strict';
    var glucoseUrineDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.glucoseUrineDeviceNodeParser', []);
    glucoseUrineDeviceNodeParser.service('glucoseUrineDeviceNodeParser', function ($templateCache, parserUtils) {
        var parseNode = function (node) {
            var nodeModel = {
                heading: node.text,
                measurementSelections: [
                    'URINE_LEVEL_NEGATIVE', 'URINE_LEVEL_PLUS_ONE',
                    'URINE_LEVEL_PLUS_TWO', 'URINE_LEVEL_PLUS_THREE',
                    'URINE_LEVEL_PLUS_FOUR'
                ]
            };
            var leftButton = {
                text: "Omit",
                nextNodeId: node.nextFail
            };
            var nodeName = node.glucoseUrine.name;
            var formName = 'inputForm_' + parserUtils.hashCode(nodeName);
            var rightButton = {
                text: "Next",
                nextNodeId: node.next,
                clickAction: function (scope) {
                    var radix = 10;
                    scope.outputModel[nodeName] = {
                        name: nodeName,
                        type: node.glucoseUrine.type,
                        value: parseInt(scope.nodeModel.measurement, radix)
                    };
                },
                validate: function (scope) { return scope[formName].$dirty; }
            };
            var template = parserUtils.getNodeTemplate('urineLevel.html');
            template = parserUtils.replaceAll(template, '#form_name#', formName);
            var representation = {
                nodeTemplate: template,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        return parseNode;
    });
}());
//# sourceMappingURL=urineGlucoseDeviceNodeParser.js.map