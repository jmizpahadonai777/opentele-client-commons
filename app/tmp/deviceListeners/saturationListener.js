(function () {
    'use strict';
    var saturationListener = angular.module('opentele-commons.deviceListeners.saturation', [
        'opentele-commons.deviceListeners'
    ]);
    saturationListener.service('saturationListener', function (listenerConstants, deviceListener) {
        var SATURATION = 'saturation';
        var PULSE = "pulse";
        var handleMeasurementEvent = function (model, event) {
            var type = event.type;
            switch (type) {
                case SATURATION:
                    var saturationEvent = event;
                    var saturation = saturationEvent.value;
                    model.saturation = saturation;
                    break;
                case PULSE:
                    var pulseEvent = event;
                    var pulse = pulseEvent.value;
                    model.pulse = pulse;
                    break;
                default:
                    console.log("Unknown measurement type: " + type);
                    break;
            }
        };
        var create = function (model) {
            return function (event) {
                deviceListener.eventListener(model, event, handleMeasurementEvent);
            };
        };
        return {
            create: create
        };
    });
}());
//# sourceMappingURL=saturationListener.js.map