(function () {
    'use strict';
    var saturationWithoutPulseDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.saturationWithoutPulseDeviceNodeParser', [
        'opentele-commons.deviceListeners'
    ]);
    var saturationWithoutPulseDeviceNodeParserService = function ($templateCache, parserUtils, nativeService, saturationListener) {
        var OMIT = 'Omit';
        var NEXT = 'Next';
        var SATURATION = "saturation";
        var generateRepresentation = function (node, nodeModel) {
            var nodeTemplate = parserUtils.getNodeTemplate('saturationWithoutPulseDeviceNode.html');
            var leftButton = {
                text: OMIT,
                nextNodeId: node.nextFail
            };
            var rightButton = {
                text: NEXT,
                nextNodeId: node.next,
                validate: function (scope) {
                    var isValueEntered = function () { return (scope.nodeModel.saturation !== undefined); };
                    return isValueEntered();
                },
                clickAction: function (scope) {
                    var saturationName = node.saturation.name;
                    var saturationType = node.saturation.type;
                    var saturationValue = scope.nodeModel.saturation;
                    scope.outputModel[saturationName] = {
                        name: saturationName,
                        type: saturationType,
                        value: saturationValue
                    };
                    var deviceName = node.deviceId.name;
                    var deviceType = node.deviceId.type;
                    var deviceId = scope.nodeModel.deviceId;
                    scope.outputModel[deviceName] = {
                        name: deviceName,
                        type: deviceType,
                        value: deviceId
                    };
                }
            };
            var representation = {
                nodeTemplate: nodeTemplate,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        var parseNode = function (node, nodeMap, outputModel) {
            var nodeModel = {
                heading: node.text
            };
            var eventListener = saturationListener.create(nodeModel, true);
            var nativeEventCallback = function (message) {
                if (message.measurementType !== SATURATION) {
                    return;
                }
                eventListener(message.event);
            };
            nativeService.subscribeToMultipleMessages('deviceMeasurementResponse', nativeEventCallback);
            nativeService.addDeviceListener(SATURATION);
            var representation = generateRepresentation(node, nodeModel);
            return representation;
        };
        return parseNode;
    };
    saturationWithoutPulseDeviceNodeParser.service('saturationWithoutPulseDeviceNodeParser', saturationWithoutPulseDeviceNodeParserService);
}());
//# sourceMappingURL=saturationWithoutPulseDeviceNodeParser.js.map