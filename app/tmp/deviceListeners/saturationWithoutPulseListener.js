(function () {
    'use strict';
    var saturationListener = angular.module('opentele-commons.deviceListeners.saturationWithoutPulse', [
        'opentele-commons.deviceListeners'
    ]);
    saturationListener.service('saturationWithoutPulseListener', function (listenerConstants, deviceListener) {
        var SATURATION = 'saturation';
        var PULSE = "pulse";
        var handleMeasurementEvent = function (model, event) {
            var type = event.type;
            switch (type) {
                case SATURATION:
                    var saturationEvent = event;
                    var saturation = saturationEvent.value;
                    model.saturation = saturation;
                    break;
                case PULSE:
                    break;
                default:
                    console.log("Unknown measurement type: " + type);
                    break;
            }
        };
        var create = function (model) {
            return function (event) {
                deviceListener.eventListener(model, event, handleMeasurementEvent);
            };
        };
        return {
            create: create
        };
    });
}());
//# sourceMappingURL=saturationWithoutPulseListener.js.map