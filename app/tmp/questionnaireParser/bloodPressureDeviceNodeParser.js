(function () {
    'use strict';
    var bloodPressureDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.bloodPressureDeviceNodeParser', [
        'opentele-commons.deviceListeners'
    ]);
    var bloodPressureDeviceNodeParserService = function ($templateCache, parserUtils, nativeService, bloodPressureListener) {
        var OMIT = 'Omit';
        var NEXT = 'Next';
        var BLOOD_PRESSURE = 'blood pressure';
        var nodeTemplate = parserUtils.getNodeTemplate('bloodPressureDeviceNode.html');
        var validate = function (scope) {
            var isValueEntered = function () {
                return (scope.nodeModel.systolic !== undefined) &&
                    (scope.nodeModel.diastolic !== undefined) &&
                    (scope.nodeModel.pulse !== undefined);
            };
            return isValueEntered();
        };
        var generateRepresentation = function (node, nodeModel) {
            var clickAction = function (scope) {
                var pulseName = node.pulse.name;
                var pulseType = node.pulse.type;
                var pulseValue = scope.nodeModel.pulse;
                scope.outputModel[pulseName] = {
                    name: pulseName,
                    type: pulseType,
                    value: pulseValue
                };
                var systolicName = node.systolic.name;
                var systolicType = node.systolic.type;
                var systolicValue = scope.nodeModel.systolic;
                scope.outputModel[systolicName] = {
                    name: systolicName,
                    type: systolicType,
                    value: systolicValue
                };
                var diastolicName = node.diastolic.name;
                var diastolicType = node.diastolic.type;
                var diastolicValue = scope.nodeModel.diastolic;
                scope.outputModel[diastolicName] = {
                    name: diastolicName,
                    type: diastolicType,
                    value: diastolicValue
                };
                var meanArterialPressureName = node.meanArterialPressure.name;
                var meanArterialPressureType = node.meanArterialPressure.type;
                var meanArterialPressureValue = scope.nodeModel.meanArterialPressure;
                scope.outputModel[meanArterialPressureName] = {
                    name: meanArterialPressureName,
                    type: meanArterialPressureType,
                    value: meanArterialPressureValue
                };
                var deviceName = node.deviceId.name;
                var deviceType = node.deviceId.type;
                var deviceId = scope.nodeModel.deviceId;
                scope.outputModel[deviceName] = {
                    name: deviceName,
                    type: deviceType,
                    value: deviceId
                };
            };
            var leftButton = {
                text: OMIT,
                nextNodeId: node.nextFail
            };
            var rightButton = {
                text: NEXT,
                nextNodeId: node.next,
                validate: validate,
                clickAction: clickAction
            };
            var representation = {
                nodeTemplate: nodeTemplate,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        var parseNode = function (node, nodeMap, outputModel) {
            var nodeModel = {
                heading: node.text,
                info: 'BLOOD_PRESSURE_CONNECT'
            };
            var eventListener = bloodPressureListener.create(nodeModel);
            var nativeEventCallback = function (message) {
                if (message.measurementType !== BLOOD_PRESSURE) {
                    return;
                }
                eventListener(message.event);
            };
            nativeService.subscribeToMultipleMessages('deviceMeasurementResponse', nativeEventCallback);
            nativeService.addDeviceListener(BLOOD_PRESSURE);
            var representation = generateRepresentation(node, nodeModel);
            return representation;
        };
        return parseNode;
    };
    bloodPressureDeviceNodeParser.service('bloodPressureDeviceNodeParser', bloodPressureDeviceNodeParserService);
}());
//# sourceMappingURL=bloodPressureDeviceNodeParser.js.map