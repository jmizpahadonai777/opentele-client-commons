(function () {
    'use strict';
    var bloodPressureListener = angular.module('opentele-commons.deviceListeners.bloodPressure', [
        'opentele-commons.deviceListeners'
    ]);
    bloodPressureListener.service('bloodPressureListener', function (listenerConstants, deviceListener) {
        var BLOOD_PRESSURE = "blood pressure";
        var PULSE = "pulse";
        var handleMeasurementEvent = function (model, event) {
            var type = event.type;
            switch (type) {
                case BLOOD_PRESSURE:
                    var bloodPressureEvent = event;
                    var bloodPressure = bloodPressureEvent.value;
                    model.systolic = bloodPressure.systolic;
                    model.diastolic = bloodPressure.diastolic;
                    model.meanArterialPressure = event.value.meanArterialPressure;
                    break;
                case PULSE:
                    var pulseEvent = event;
                    var pulse = pulseEvent.value;
                    model.pulse = pulse;
                    break;
                default:
                    throw new TypeError("Unknown measurement event type received: " + type);
            }
        };
        var create = function (model) {
            return function (event) {
                deviceListener.eventListener(model, event, handleMeasurementEvent);
            };
        };
        return {
            create: create
        };
    });
}());
//# sourceMappingURL=bloodPressureListener.js.map