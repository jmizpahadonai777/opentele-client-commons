(function () {
    'use strict';
    var haemoglobinDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.haemoglobinDeviceNodeParser', []);
    haemoglobinDeviceNodeParser.service('haemoglobinDeviceNodeParser', function (parserUtils) {
        var parseNode = function (node) {
            return parserUtils.parseSimpleInputNode(node, node.haemoglobinValue, 'HAEMOGLOBIN');
        };
        return parseNode;
    });
}());
//# sourceMappingURL=haemoglobinDeviceNodeParser.js.map