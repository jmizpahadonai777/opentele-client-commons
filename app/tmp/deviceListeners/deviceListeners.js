(function () {
    'use strict';
    var listeners = angular.module('opentele-commons.deviceListeners', [
        'opentele-commons.deviceListeners.bloodPressure',
        'opentele-commons.deviceListeners.saturation',
        'opentele-commons.deviceListeners.saturationWithoutPulse',
        'opentele-commons.deviceListeners.weight',
        'opentele-commons.deviceListeners.templates'
    ]);
    listeners.constant('listenerConstants', {
        MEASUREMENT: 'measurement',
        STATUS: 'status',
        INFO: 'info',
        ERROR: 'error',
        DEVICE: 'device'
    });
    listeners.service('deviceListener', function (listenerConstants) {
        var statusEventHandlers = {};
        statusEventHandlers[listenerConstants.INFO] = function (model, event) {
            model.info = event.message;
        };
        statusEventHandlers[listenerConstants.ERROR] = function (model, event) {
            model.error = event.message;
        };
        var handleStatusEvent = function (model, event) {
            var type = event.type;
            if (!statusEventHandlers.hasOwnProperty(type)) {
                console.log("Unknown status type: " + type);
                return;
            }
            statusEventHandlers[type](model, event);
        };
        var eventListener = function (model, event, measurementHandler) {
            var type = event.type;
            if (event.hasOwnProperty(listenerConstants.DEVICE)) {
                var device = event.device;
                model.deviceId = device.systemId;
            }
            switch (type) {
                case listenerConstants.MEASUREMENT:
                    var measurementEvent = event[type];
                    measurementHandler(model, measurementEvent);
                    break;
                case listenerConstants.STATUS:
                    var statusEvent = event[type];
                    handleStatusEvent(model, statusEvent);
                    break;
                default:
                    console.log("Unknown event type: " + type);
                    break;
            }
        };
        var listener = {
            overrideStatusEventHandler: function (eventType, handler) {
                statusEventHandlers[eventType] = handler;
            },
            eventListener: eventListener
        };
        return listener;
    });
}());
//# sourceMappingURL=deviceListeners.js.map