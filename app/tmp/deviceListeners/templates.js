angular.module('opentele-commons.deviceListeners.templates', ['deviceListeners/measurementTemplates/bloodPressure.html', 'deviceListeners/measurementTemplates/lungFunction.html', 'deviceListeners/measurementTemplates/saturation.html']);

angular.module("deviceListeners/measurementTemplates/bloodPressure.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("deviceListeners/measurementTemplates/bloodPressure.html",
    "<div class=\"center-div\">\n" +
    "    <h4 class=\"line-wrap\">{{ model.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"bloodPressureForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"blood-pressure-systolic\">\n" +
    "                {{ \"BLOOD_PRESSURE_SYSTOLIC\" | translate }}\n" +
    "            </label>\n" +
    "            <input id=\"blood-pressure-systolic\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"blood-pressure-systolic\"\n" +
    "                   ng-model=\"model.systolic\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"blood-pressure-diastolic\">\n" +
    "                {{ \"BLOOD_PRESSURE_DIASTOLIC\" | translate }}\n" +
    "            </label>\n" +
    "            <input id=\"blood-pressure-diastolic\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"blood-pressure-diastolic\"\n" +
    "                   ng-model=\"model.diastolic\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"blood-pressure-pulse\">\n" +
    "                {{ \"BLOOD_PRESSURE_PULSE\" | translate }}\n" +
    "            </label>\n" +
    "            <input id=\"blood-pressure-pulse\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"blood-pressure-pulse\"\n" +
    "                   ng-model=\"model.pulse\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"block\"\n" +
    "             ng-show=\"model.error !== undefined\">\n" +
    "            <small class=\"error-message\">\n" +
    "                {{ model.error | translate }}\n" +
    "            </small>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("deviceListeners/measurementTemplates/lungFunction.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("deviceListeners/measurementTemplates/lungFunction.html",
    "<div class=\"center-div\">\n" +
    "    <h4 class=\"line-wrap\">{{ model.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"lungFunctionForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"lung-function-fev1\">\n" +
    "                {{ \"LUNG_FUNCTION_FEV1\" | translate }}\n" +
    "            </label>\n" +
    "            <input id=\"lung-function-fev1\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"lung-function-fev1\"\n" +
    "                   ng-model=\"model.fev1\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"block\"\n" +
    "             ng-show=\"model.error !== undefined\">\n" +
    "            <small class=\"error-message\">\n" +
    "                {{ model.error | translate }}\n" +
    "            </small>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("deviceListeners/measurementTemplates/saturation.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("deviceListeners/measurementTemplates/saturation.html",
    "<div class=\"center-div\">\n" +
    "    <h4 class=\"line-wrap\">{{ model.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"saturationForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"saturation-saturation\">\n" +
    "                {{ \"SATURATION_SATURATION\" | translate }}\n" +
    "            </label>\n" +
    "            <input id=\"saturation-saturation\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"saturation-saturation\"\n" +
    "                   ng-model=\"model.saturation\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"saturation-pulse\">\n" +
    "                {{ \"SATURATION_PULSE\" | translate }}\n" +
    "            </label>\n" +
    "            <input id=\"saturation-pulse\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"saturation-pulse\"\n" +
    "                   ng-model=\"model.pulse\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"block\"\n" +
    "             ng-show=\"model.error !== undefined\">\n" +
    "            <small class=\"error-message\">\n" +
    "                {{ model.error | translate }}\n" +
    "            </small>\n" +
    "        </div>\n" +
    "\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);
