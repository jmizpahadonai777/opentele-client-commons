(function () {
    'use strict';
    var lungMonitorDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.lungMonitorDeviceNodeParser', [
        'opentele-commons.deviceListeners'
    ]);
    var lungMonitorDeviceNodeParserService = function ($templateCache, parserUtils, nativeService, lungFunctionListener) {
        var OMIT = 'Omit';
        var NEXT = 'Next';
        var LUNG_FUNCTION = 'lung function';
        var nodeTemplate = parserUtils.getNodeTemplate('lungMonitorDeviceNode.html');
        var validate = function (scope) {
            var isValueEntered = function () {
                return (scope.nodeModel.fev1 !== undefined) &&
                    (scope.nodeModel.fev6 !== undefined) &&
                    (scope.nodeModel.fev1Fev6Ratio !== undefined) &&
                    (scope.nodeModel.goodTest !== undefined) &&
                    (scope.nodeModel.softwareVersion !== undefined);
            };
            return isValueEntered();
        };
        var generateRepresentation = function (node, nodeModel) {
            var clickAction = function (scope) {
                var measurements = [
                    'fev1', 'fev6', 'fev1Fev6Ratio', 'fef2575',
                    'goodTest', 'softwareVersion', 'deviceId'
                ];
                measurements.forEach(function (measurement) {
                    var measurementName = node[measurement].name;
                    var measurementType = node[measurement].type;
                    var measurementValue = scope.nodeModel[measurement];
                    scope.outputModel[measurementName] = {
                        name: measurementName,
                        type: measurementType,
                        value: measurementValue
                    };
                });
            };
            var leftButton = {
                text: OMIT,
                nextNodeId: node.nextFail
            };
            var rightButton = {
                text: NEXT,
                nextNodeId: node.next,
                validate: validate,
                clickAction: clickAction
            };
            var representation = {
                nodeTemplate: nodeTemplate,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        var parseNode = function (node, nodeMap, outputModel) {
            var html5HookDescription = {
                elementId: 'deviceHook',
                modelName: 'nodeModel',
                callbackName: 'eventListener'
            };
            var nodeModel = {
                heading: node.text,
                info: 'CONNECTING'
            };
            var eventListener = lungFunctionListener.create(nodeModel);
            nodeModel.eventListener = eventListener;
            nativeService.addDeviceListener(LUNG_FUNCTION, html5HookDescription);
            var representation = generateRepresentation(node, nodeModel);
            return representation;
        };
        return parseNode;
    };
    lungMonitorDeviceNodeParser.service('lungMonitorDeviceNodeParser', lungMonitorDeviceNodeParserService);
}());
//# sourceMappingURL=lungMonitorDeviceNodeParser.js.map