angular.module('opentele-commons.questionnaireParser.templates', ['questionnaireParser/nodeTemplates/bloodPressureDeviceNode.html', 'questionnaireParser/nodeTemplates/bloodSugarManualDeviceNode.html', 'questionnaireParser/nodeTemplates/crpNode.html', 'questionnaireParser/nodeTemplates/delayNode.html', 'questionnaireParser/nodeTemplates/ioNodeEditText.html', 'questionnaireParser/nodeTemplates/ioNodeRadioButton.html', 'questionnaireParser/nodeTemplates/ioNodeText.html', 'questionnaireParser/nodeTemplates/lungMonitorDeviceNode.html', 'questionnaireParser/nodeTemplates/saturationDeviceNode.html', 'questionnaireParser/nodeTemplates/saturationWithoutPulseDeviceNode.html', 'questionnaireParser/nodeTemplates/simpleInputNode.html', 'questionnaireParser/nodeTemplates/temperatureManualDeviceNode.html', 'questionnaireParser/nodeTemplates/urineLevel.html', 'questionnaireParser/nodeTemplates/weightDeviceNode.html']);

angular.module("questionnaireParser/nodeTemplates/bloodPressureDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/bloodPressureDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "    <h2 class=\"line-wrap\">{{ nodeModel.heading | translate }}</h2>\n" +
    "</div>\n" +
    "<div id=\"deviceHook\"></div>\n" +
    "<div class=\"center-div\">\n" +
    "    <h4 class=\"line-wrap\">{{ nodeModel.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"bloodPressureForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"blood-pressure-systolic\">{{ \"BLOOD_PRESSURE_SYSTOLIC\" | translate }}</label>\n" +
    "            <input id=\"blood-pressure-systolic\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"blood-pressure-systolic\"\n" +
    "                   ng-model=\"nodeModel.systolic\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"blood-pressure-diastolic\">{{ \"BLOOD_PRESSURE_DIASTOLIC\" | translate }}</label>\n" +
    "            <input id=\"blood-pressure-diastolic\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"blood-pressure-diastolic\"\n" +
    "                   ng-model=\"nodeModel.diastolic\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"blood-pressure-pulse\">{{ \"BLOOD_PRESSURE_PULSE\" | translate }}</label>\n" +
    "            <input id=\"blood-pressure-pulse\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"blood-pressure-pulse\"\n" +
    "                   ng-model=\"nodeModel.pulse\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "        <div class=\"block\"\n" +
    "             ng-show=\"nodeModel.error !== undefined\">\n" +
    "            <small class=\"error-message\">{{ nodeModel.error | translate }}</small>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/bloodSugarManualDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/bloodSugarManualDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "    <h2 class=\"line-wrap\">{{nodeModel.heading}}</h2>\n" +
    "</div>\n" +
    "<form name=\"bloodSugarForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "        <div>\n" +
    "            <label for=\"count\">{{ \"BLOOD_SUGAR\" | translate }}</label>\n" +
    "            <input id=\"count\"\n" +
    "                   type=\"number\"\n" +
    "                   min=\"0\"\n" +
    "                   max=\"999\"\n" +
    "                   name=\"count\"\n" +
    "                   autocomplete=\"off\"\n" +
    "                   ng-model=\"nodeModel.bloodSugarManualMeasurement\" required />\n" +
    "        </div>\n" +
    "        <div ng-show=\"bloodSugarForm.count.$dirty && bloodSugarForm.count.$invalid\">\n" +
    "            <small class=\"error-message\"\n" +
    "                   ng-show=\"bloodSugarForm.count.$error.required\">\n" +
    "                {{ \"BLOOD_SUGAR_COUNT_ERROR_MESSAGE\" | translate }}\n" +
    "            </small>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <label for=\"beforeMeal\">{{ \"BEFORE_MEAL\" | translate }}</label>\n" +
    "            <input id=\"beforeMeal\"\n" +
    "                   type=\"checkbox\"\n" +
    "                   ng-model=\"nodeModel.bloodSugarManualBeforeMeal\"/>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <label for=\"afterMeal\">{{ \"AFTER_MEAL\" | translate }}</label>\n" +
    "            <input id=\"afterMeal\"\n" +
    "                   type=\"checkbox\"\n" +
    "                   ng-model=\"nodeModel.bloodSugarManualAfterMeal\"/>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/crpNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/crpNode.html",
    "<div class=\"center-div\">\n" +
    "    <h2 class=\"line-wrap\">{{nodeModel.heading}}</h2>\n" +
    "</div>\n" +
    "<form name=\"crpForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "\n" +
    "        <div>\n" +
    "            <label for=\"lt5\">{{ \"LT5\" | translate }}</label>\n" +
    "            <input id=\"lt5\"\n" +
    "                   name=\"lt5\"\n" +
    "                   type=\"checkbox\"\n" +
    "                   ng-model=\"nodeModel.crpLt5Measurement\"/>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <label for=\"count\">{{ \"OR_CRP\" | translate }}</label>\n" +
    "            <input id=\"count\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"count\"\n" +
    "                   autocomplete=\"off\"\n" +
    "                   ng-model=\"nodeModel.crpCountMeasurement\"/>\n" +
    "        </div>\n" +
    "\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/delayNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/delayNode.html",
    "<div class=\"center-div\">\n" +
    "	<h2 class=\"line-wrap\">{{nodeModel.heading}}</h2>\n" +
    "</div>\n" +
    "<div timer=\"nodeModel.nodeId\"></div>\n" +
    "<div class=\"center-div\">\n" +
    "	<h2 class=\"line-wrap\">{{nodeModel.timerDescription}}</h2>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/ioNodeEditText.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/ioNodeEditText.html",
    "<form name=\"#form_name#\" class=\"text-center\">\n" +
    "	<fieldset class=\"questionnaire-fields\">\n" +
    "		<div class=\"block\">\n" +
    "			<label for=\"#input_name#\">#label#</label>\n" +
    "			<input id=\"#input_name#\"\n" +
    "                   type=\"#type#\"\n" +
    "                   step=\"#step#\"\n" +
    "                   name=\"#input_name#\"\n" +
    "                   autocomplete=\"off\"\n" +
    "                   ng-model=\"nodeModel.#input_name#\" required />\n" +
    "		</div>\n" +
    "		<div class=\"block\"\n" +
    "             ng-show=\"#form_name#.#input_name#.$dirty && #form_name#.#input_name#.$invalid\">\n" +
    "			<small class=\"error-message\"\n" +
    "                   ng-show=\"#form_name#.#input_name#.$error.required\">{{ \"EDIT_TEXT_ELEMENT_ERROR_MESSAGE\" | translate }}</small>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/ioNodeRadioButton.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/ioNodeRadioButton.html",
    "<form name=\"#form_name#\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "        <div class=\"narrow-row\"\n" +
    "             ng-repeat=\"radioItem in nodeModel.radioItems\">\n" +
    "            <input id=\"radio-items\"\n" +
    "                   name=\"radio-items\"\n" +
    "                   type=\"radio\"\n" +
    "                   value=\"{{radioItem.value}}\"\n" +
    "                   ng-model=\"nodeModel.radioSelected\">\n" +
    "            <div class=\"radio-label\" for=\"radio-items\">\n" +
    "              {{radioItem.label}}<br/>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/ioNodeText.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/ioNodeText.html",
    "<div class=\"center-div\">\n" +
    "  <h2 class=\"line-wrap\">#heading#</h2>\n" +
    "  <div class=\"line-wrap\">#media#</div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/lungMonitorDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/lungMonitorDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "    <h2 class=\"line-wrap\">{{ nodeModel.heading | translate }}</h2>\n" +
    "</div>\n" +
    "<div id=\"deviceHook\"></div>\n" +
    "<div class=\"center-div\">\n" +
    "    <h4 class=\"line-wrap\">{{ nodeModel.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"lungFunctionForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "        <div class=\"block\">\n" +
    "            <label for=\"lung-function-fev1\">{{ \"LUNG_FUNCTION_FEV1\" | translate }}</label>\n" +
    "            <input id=\"lung-function-fev1\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"lung-function-fev1\"\n" +
    "                   ng-model=\"nodeModel.fev1\"\n" +
    "                   disabled />\n" +
    "        </div>\n" +
    "        <div class=\"block\"\n" +
    "             ng-show=\"nodeModel.error !== undefined\">\n" +
    "            <small class=\"error-message\">{{ nodeModel.error | translate }}</small>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/saturationDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/saturationDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "	<h2 class=\"line-wrap\">{{nodeModel.heading | translate }}</h2>\n" +
    "</div>\n" +
    "<div id=\"deviceHook\"></div>\n" +
    "<div class=\"center-div\">\n" +
    "	<h4 class=\"line-wrap\">{{ nodeModel.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"saturationForm\" class=\"text-center\">\n" +
    "	<fieldset class=\"questionnaire-fields\">\n" +
    "		<div class=\"block\">\n" +
    "			<label for=\"saturation-saturation\">{{ \"SATURATION_SATURATION\" | translate }}</label>\n" +
    "			<input id=\"saturation-saturation\"\n" +
    "             type=\"number\"\n" +
    "             name=\"saturation-saturation\"\n" +
    "             ng-model=\"nodeModel.saturation\"\n" +
    "             disabled />\n" +
    "		</div>\n" +
    "		<div class=\"block\">\n" +
    "			<label for=\"saturation-pulse\">{{ \"SATURATION_PULSE\" | translate }}</label>\n" +
    "			<input id=\"saturation-pulse\"\n" +
    "             type=\"number\"\n" +
    "             name=\"saturation-pulse\"\n" +
    "             ng-model=\"nodeModel.pulse\"\n" +
    "             disabled />\n" +
    "		</div>\n" +
    "		<div class=\"block\"\n" +
    "             ng-show=\"nodeModel.error !== undefined\">\n" +
    "			<small class=\"error-message\">{{ nodeModel.error | translate }}</small>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/saturationWithoutPulseDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/saturationWithoutPulseDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "	<h2 class=\"line-wrap\">{{nodeModel.heading | translate }}</h2>\n" +
    "</div>\n" +
    "<div id=\"deviceHook\"></div>\n" +
    "<div class=\"center-div\">\n" +
    "	<h4 class=\"line-wrap\">{{ nodeModel.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"saturationForm\" class=\"text-center\">\n" +
    "	<fieldset class=\"questionnaire-fields\">\n" +
    "		<div class=\"block\">\n" +
    "			<label for=\"saturation-saturation\">{{ \"SATURATION_SATURATION\" | translate }}</label>\n" +
    "			<input id=\"saturation-saturation\"\n" +
    "             type=\"number\"\n" +
    "             name=\"saturation-saturation\"\n" +
    "             ng-model=\"nodeModel.saturation\"\n" +
    "             disabled />\n" +
    "		</div>\n" +
    "		<div class=\"block\"\n" +
    "             ng-show=\"nodeModel.error !== undefined\">\n" +
    "			<small class=\"error-message\">{{ nodeModel.error | translate }}</small>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/simpleInputNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/simpleInputNode.html",
    "<div class=\"center-div\">\n" +
    "	<h2 class=\"line-wrap\">{{nodeModel.heading}}</h2>\n" +
    "</div>\n" +
    "<form name=\"inputForm\" class=\"text-center\">\n" +
    "	<fieldset class=\"questionnaire-fields\">\n" +
    "		<div>\n" +
    "			<label for=\"value\">{{ \"#field_name#\" | translate }}</label>\n" +
    "			<input id=\"value\"\n" +
    "				type=\"number\"\n" +
    "				min=\"0\"\n" +
    "				max=\"999\"\n" +
    "				name=\"value\"\n" +
    "                autocomplete=\"off\"\n" +
    "				ng-model=\"nodeModel.measurement\" required ng-pattern=\"#pattern#\"/>\n" +
    "		</div>\n" +
    "		<div ng-show=\"inputForm.value.$dirty && inputForm.value.$invalid\">\n" +
    "			<small class=\"error-message\"\n" +
    "				ng-show=\"inputForm.value.$error.required\">\n" +
    "				{{ \"#field_name#_ERROR_MESSAGE\" | translate }}\n" +
    "			</small>\n" +
    "			<small class=\"error-message\"\n" +
    "				ng-show=\"inputForm.value.$error.pattern\">\n" +
    "				{{ \"#field_name#_ERROR_MESSAGE\" | translate }}\n" +
    "			</small>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/temperatureManualDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/temperatureManualDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "    <h2 class=\"line-wrap\">{{nodeModel.heading}}</h2>\n" +
    "</div>\n" +
    "<form name=\"temperatureForm\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "        <div>\n" +
    "            <label for=\"temperature\">{{ \"TEMPERATURE\" | translate }}</label>\n" +
    "            <input id=\"temperature\"\n" +
    "                   type=\"number\"\n" +
    "                   name=\"temperature\"\n" +
    "                   autocomplete=\"off\"\n" +
    "                   ng-model=\"nodeModel.temperatureMeasurement\" required />\n" +
    "        </div>\n" +
    "        <div ng-show=\"temperatureForm.temperature.$dirty && temperatureForm.temperature.$invalid\">\n" +
    "            <small class=\"error-message\"\n" +
    "                   ng-show=\"temperatureForm.temperature.$error.required\">\n" +
    "                {{ \"TEMPERATURE_ERROR_MESSAGE\" | translate }}\n" +
    "            </small>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/urineLevel.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/urineLevel.html",
    "<div class=\"center-div\">\n" +
    "    <h2 class=\"line-wrap\">{{nodeModel.heading}}</h2>\n" +
    "</div>\n" +
    "<form name=\"#form_name#\" class=\"text-center\">\n" +
    "    <fieldset class=\"questionnaire-fields\">\n" +
    "        <div>\n" +
    "            <ul>\n" +
    "                <li class=\"narrow-row list-unstyled\"\n" +
    "                    ng-repeat=\"level in nodeModel.measurementSelections\">\n" +
    "                    <input id=\"radio-{{$index}}\"\n" +
    "                           name=\"radio-{{$index}}\"\n" +
    "                           type=\"radio\"\n" +
    "                           ng-model=\"nodeModel.measurement\"\n" +
    "                           value=\"{{$index}}\">\n" +
    "                    <div class=\"radio-label\">\n" +
    "                        {{ level | translate}}<br/>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </fieldset>\n" +
    "</form>\n" +
    "");
}]);

angular.module("questionnaireParser/nodeTemplates/weightDeviceNode.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("questionnaireParser/nodeTemplates/weightDeviceNode.html",
    "<div class=\"center-div\">\n" +
    "	<h2 class=\"line-wrap\">{{nodeModel.heading | translate }}</h2>\n" +
    "</div>\n" +
    "<div id=\"deviceHook\"></div>\n" +
    "<div class=\"center-div\">\n" +
    "	<h4 class=\"line-wrap\">{{ nodeModel.info | translate }}</h4>\n" +
    "</div>\n" +
    "<form name=\"weightForm\" class=\"text-center\">\n" +
    "	<fieldset class=\"questionnaire-fields\">\n" +
    "		<div class=\"block\">\n" +
    "			<label for=\"weight-measurement\">{{ \"WEIGHT\" | translate }}</label>\n" +
    "			<input id=\"weight-measurement\"\n" +
    "             type=\"number\"\n" +
    "             name=\"weight-measurement\"\n" +
    "             ng-model=\"nodeModel.weight\"\n" +
    "             disabled />\n" +
    "		</div>\n" +
    "		<div class=\"block\"\n" +
    "             ng-show=\"nodeModel.error !== undefined\">\n" +
    "			<small class=\"error-message\">{{ nodeModel.error | translate }}</small>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</form>\n" +
    "");
}]);
