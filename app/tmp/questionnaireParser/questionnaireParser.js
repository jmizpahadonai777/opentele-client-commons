(function () {
    'use strict';
    var questionnaireParser = angular.module('opentele-commons.questionnaireParser', [
        'opentele-commons.questionnaireParser.templates',
        'opentele-commons.questionnaireParser.utils',
        'opentele-commons.questionnaireParser.haemoglobinDeviceNodeParser',
        'opentele-commons.questionnaireParser.bloodSugarManualDeviceNodeParser',
        'opentele-commons.questionnaireParser.crpNodeParser',
        'opentele-commons.questionnaireParser.ioNodeParser',
        'opentele-commons.questionnaireParser.assignmentNodeParser',
        'opentele-commons.questionnaireParser.endNodeParser',
        'opentele-commons.questionnaireParser.decisionNodeParser',
        'opentele-commons.questionnaireParser.temperatureManualDeviceNodeParser',
        'opentele-commons.questionnaireParser.urineDeviceNodeParser',
        'opentele-commons.questionnaireParser.glucoseUrineDeviceNodeParser',
        'opentele-commons.questionnaireParser.bloodUrineDeviceNodeParser',
        'opentele-commons.questionnaireParser.nitriteUrineDeviceNodeParser',
        'opentele-commons.questionnaireParser.leukocytesUrineDeviceNodeParser',
        'opentele-commons.questionnaireParser.delayNodeParser',
        'opentele-commons.questionnaireParser.weightDeviceNodeParser',
        'opentele-commons.questionnaireParser.bloodPressureDeviceNodeParser',
        'opentele-commons.questionnaireParser.saturationDeviceNodeParser',
        'opentele-commons.questionnaireParser.saturationWithoutPulseDeviceNodeParser'
    ]);
    questionnaireParser.service('nodesParser', function ($injector, parserUtils) {
        var getParser = function (nodeType) {
            var firstNonUpperCaseCharacter = function (str) {
                for (var i = 0; i < str.length; i++) {
                    var c = str[i];
                    if (!('A' <= c && c <= 'Z')) {
                        if (i > 1) {
                            return i - 1;
                        }
                        else {
                            return i;
                        }
                    }
                }
                return -1;
            };
            var idx = firstNonUpperCaseCharacter(nodeType);
            var parserName = nodeType.slice(0, idx).toLowerCase() + nodeType.slice(idx) + 'Parser';
            return $injector.get(parserName);
        };
        var hasParser = function (nodeType) {
            try {
                getParser(nodeType);
                return true;
            }
            catch (e) {
                return false;
            }
        };
        var parseNode = function (currentNodeId, nodeMap, outputModel) {
            var nodeToParse = nodeMap[currentNodeId];
            var nodeType = parserUtils.getNodeType(nodeToParse);
            if (!hasParser(nodeType)) {
                throw new TypeError('Node of type ' + nodeType + ' not supported');
            }
            var toRepresentation = getParser(nodeType);
            var parsed = toRepresentation(nodeToParse[nodeType], nodeMap, outputModel);
            if (!parsed.hasOwnProperty('nodeId')) {
                parsed.nodeId = nodeToParse[nodeType].nodeName;
            }
            return parsed;
        };
        var validateNodes = function (nodeMap) {
            var errorTypes = [];
            var nodes = [];
            for (var nodeId in nodeMap) {
                if (nodeMap.hasOwnProperty(nodeId)) {
                    nodes.push(nodeMap[nodeId]);
                }
            }
            if (nodes === null || nodes.length === 0) {
                throw new TypeError('Questionnaire Node list was empty or null.');
            }
            for (var i = 0; i < nodes.length; i++) {
                var node = nodes[i];
                var nodeType = parserUtils.getNodeType(node);
                if (!hasParser(nodeType)) {
                    errorTypes.push(nodeType);
                }
            }
            if (errorTypes.length > 0) {
                var error = new TypeError('The following Node types are not supported: ' + errorTypes);
                throw error;
            }
        };
        var parser = {
            parse: function (currentNodeId, nodeMap, outputModel) {
                return parseNode(currentNodeId, nodeMap, outputModel);
            },
            validate: function (nodeMap) {
                return validateNodes(nodeMap);
            }
        };
        return parser;
    });
}());
//# sourceMappingURL=questionnaireParser.js.map