(function () {
    'use strict';
    var parserUtils = angular.module('opentele-commons.questionnaireParser.utils', []);
    parserUtils.service('parserUtils', function ($templateCache) {
        var getFirstKeyFromLiteral = function (literal) {
            for (var key in literal) {
                if (literal.hasOwnProperty(key)) {
                    return key;
                }
            }
        };
        var getNodeTemplate = function (templateName) {
            var template = $templateCache.get('questionnaireParser/nodeTemplates/' + templateName);
            if (typeof template === 'undefined') {
                throw new Error('HTML template does not exist for ' + templateName);
            }
            return template;
        };
        var getNodeType = function (node) { return getFirstKeyFromLiteral(node); };
        var hashCode = function (str) {
            var hash = 0, i, chr, len;
            if (str.length === 0) {
                return hash;
            }
            for (i = 0, len = str.length; i < len; i++) {
                chr = str.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0;
            }
            return Math.abs(hash);
        };
        var replaceAll = function (str, find, replace) {
            var escapeRegExp = function (str) {
                return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
            };
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        };
        var parseSimpleInputNode = function (node, nodeValueEntry, fieldName) {
            var isIntegerNode = nodeValueEntry.type === 'Integer';
            var template = getNodeTemplate('simpleInputNode.html');
            template = replaceAll(template, '#field_name#', fieldName);
            var numberPattern = isIntegerNode === true ? /^\d+$/ : /.+/;
            template = replaceAll(template, '#pattern#', numberPattern);
            var representation = {
                nodeTemplate: template,
                nodeModel: {
                    heading: node.text
                }
            };
            representation.rightButton = {
                text: "Next",
                nextNodeId: node.next,
                validate: function (scope) {
                    return scope.inputForm.value.$valid;
                },
                clickAction: function (scope) {
                    var nodeName = nodeValueEntry.name;
                    scope.outputModel[nodeName] = {
                        name: nodeName,
                        type: nodeValueEntry.type,
                        value: scope.nodeModel.measurement
                    };
                }
            };
            representation.leftButton = {
                text: "Omit",
                nextNodeId: node.nextFail
            };
            return representation;
        };
        return {
            getFirstKeyFromLiteral: getFirstKeyFromLiteral,
            getNodeTemplate: getNodeTemplate,
            getNodeType: getNodeType,
            hashCode: hashCode,
            parseSimpleInputNode: parseSimpleInputNode,
            replaceAll: replaceAll
        };
    });
}());
//# sourceMappingURL=parserUtils.js.map