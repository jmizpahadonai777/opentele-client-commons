(function () {
    'use strict';
    var temperatureDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.temperatureManualDeviceNodeParser', []);
    var temperatureService = function ($templateCache, parserUtils) {
        var parseNode = function (node) {
            var nodeModel = {
                heading: node.text
            };
            var leftButton = {
                text: "Omit",
                nextNodeId: node.nextFail
            };
            var rightButton = {
                text: "Next",
                nextNodeId: node.next,
                validate: function (scope) { return scope.temperatureForm.temperature.$valid; },
                clickAction: function (scope) {
                    var nodeName = node.temperature.name;
                    scope.outputModel[nodeName] = {
                        name: nodeName,
                        type: node.temperature.type,
                        value: scope.nodeModel.temperatureMeasurement
                    };
                }
            };
            var representation = {
                nodeTemplate: parserUtils.getNodeTemplate('temperatureManualDeviceNode.html'),
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        return parseNode;
    };
    temperatureDeviceNodeParser.service('temperatureManualDeviceNodeParser', temperatureService);
}());
//# sourceMappingURL=temperatureManualDeviceNodeParser.js.map