(function () {
    'use strict';
    var weightListener = angular.module('opentele-commons.deviceListeners.weight', [
        'opentele-commons.deviceListeners'
    ]);
    weightListener.service('weightListener', function (listenerConstants, deviceListener) {
        var handleMeasurementEvent = function (model, event) {
            var weightEvent = event;
            var weight = weightEvent.value;
            model.weight = weight;
        };
        var create = function (model) {
            return function (event) {
                deviceListener.eventListener(model, event, handleMeasurementEvent);
            };
        };
        return {
            create: create
        };
    });
}());
//# sourceMappingURL=weightListener.js.map