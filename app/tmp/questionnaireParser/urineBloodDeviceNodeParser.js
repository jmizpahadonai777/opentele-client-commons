(function () {
    'use strict';
    var bloodUrineDeviceNodeParser = angular.module('opentele-commons.questionnaireParser.bloodUrineDeviceNodeParser', []);
    var service = function ($templateCache, parserUtils) {
        var parseNode = function (node) {
            var nodeModel = {
                heading: node.text,
                measurementSelections: [
                    'URINE_LEVEL_NEGATIVE', 'URINE_LEVEL_PLUS_MINUS',
                    'URINE_LEVEL_PLUS_ONE', 'URINE_LEVEL_PLUS_TWO',
                    'URINE_LEVEL_PLUS_THREE'
                ]
            };
            var leftButton = {
                text: "Omit",
                nextNodeId: node.nextFail
            };
            var nodeName = node.bloodUrine.name;
            var formName = 'inputForm_' + parserUtils.hashCode(nodeName);
            var rightButton = {
                text: "Next",
                nextNodeId: node.next,
                clickAction: function (scope) {
                    var radix = 10;
                    scope.outputModel[nodeName] = {
                        name: nodeName,
                        type: node.bloodUrine.type,
                        value: parseInt(scope.nodeModel.measurement, radix)
                    };
                },
                validate: function (scope) { return scope[formName].$dirty; }
            };
            var template = parserUtils.getNodeTemplate('urineLevel.html');
            template = parserUtils.replaceAll(template, '#form_name#', formName);
            var representation = {
                nodeTemplate: template,
                nodeModel: nodeModel,
                leftButton: leftButton,
                rightButton: rightButton
            };
            return representation;
        };
        return parseNode;
    };
    bloodUrineDeviceNodeParser.service('bloodUrineDeviceNodeParser', service);
}());
//# sourceMappingURL=urineBloodDeviceNodeParser.js.map